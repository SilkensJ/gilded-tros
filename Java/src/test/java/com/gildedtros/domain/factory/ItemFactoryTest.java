package com.gildedtros.domain.factory;

import com.gildedtros.domain.exception.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemFactoryTest {

    @Test
    void when_BadWineIsCreated_then_exception() {
        Assertions.assertThrows(ValidationException.class, () -> ItemFactory.createWine("BAD WINE" , 10 , -10));
    }

    @Test
    void when_BadGeneralItemIsCreated_then_exception() {
        Assertions.assertThrows(ValidationException.class, () -> ItemFactory.createGeneralItem(null, 10 , 10));
    }

    @Test
    void when_BadBackstagePasIsCreated_then_exception() {
        Assertions.assertThrows(ValidationException.class, () -> ItemFactory.createBackstagePass(null, 10 , 10));
    }

    @Test
    void when_BadSmellyItemPasIsCreated_then_exception() {
        Assertions.assertThrows(ValidationException.class, () -> ItemFactory.createSmellyItem(null, 10 , -10));
    }
}
package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.GeneralItem;
import com.gildedtros.domain.model.SmellyItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.gildedtros.domain.factory.ItemFactory.createGeneralItem;
import static com.gildedtros.domain.factory.ItemFactory.createSmellyItem;

class GeneralItemManagerTest {
    public GeneralItemManager generalItemManager = new GeneralItemManager();

    @Test
    @DisplayName("When a general item sell date is 20 days away then quality degrades once as fast when a day passes")
    void when_GeneralItemToSellIn20Days_then_decreaseQualityOnce() {
        final GeneralItem generalItem = createGeneralItem("Macbook PRO", 20, 20);
        final GeneralItem generalItemExpected = createGeneralItem("Macbook PRO", 19, 19);

        final Item actualSmellyItem = generalItemManager.manage(generalItem);

        Assertions.assertEquals(generalItemExpected, actualSmellyItem);
    }

    @Test
    @DisplayName("When a general item has a quality of 0 then don't degrade quality")
    void when_GeneralItemHasQualityOfZero_then_dontDegradeQuality() {
        final GeneralItem generalItem = createGeneralItem("Macbook PRO", 20, 0);
        final GeneralItem generalItemExpected = createGeneralItem("Macbook PRO", 19, 0);

        final Item actualGeneralItem = generalItemManager.manage(generalItem);

        Assertions.assertEquals(generalItemExpected, actualGeneralItem);
    }

    @Test
    @DisplayName("When another item than a general item is passed then throw exception")
    void when_SmellyItemIsGiven_then_throwException() {
        final SmellyItem invalid = createSmellyItem("INVALID", 20, 20);

        Assertions.assertThrows(IllegalArgumentException.class, () -> generalItemManager.manage(invalid));

    }
}
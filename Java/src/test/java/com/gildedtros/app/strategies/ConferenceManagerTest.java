package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.BackstagePass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.gildedtros.domain.factory.ItemFactory.createBackstagePass;

class ConferenceManagerTest {

    public ConferenceManager conferenceManager = new ConferenceManager();

    @Test
    @DisplayName("When conference is 20 days away then degrade quality")
    void when_ConferenceIsTwentyDaysAway_then_DegradeQuality() {
        final BackstagePass conference = createBackstagePass("DEVOXX ANTWERP", 20, 20);
        final BackstagePass conferenceExpected = createBackstagePass("DEVOXX ANTWERP", 19, 19);

        final Item actualConference = conferenceManager.manage(conference);

        Assertions.assertEquals(conferenceExpected, actualConference);
    }

    @Test
    @DisplayName("When conference is 9 days away then increase quality with two")
    void when_ConferenceIsNineDaysAway_then_IncreaseQualityByTwo() {
        final BackstagePass conference = createBackstagePass("DEVOXX ANTWERP", 9, 20);
        final BackstagePass conferenceExpected = createBackstagePass("DEVOXX ANTWERP", 8, 22);

        final Item actualConference = conferenceManager.manage(conference);

        Assertions.assertEquals(conferenceExpected, actualConference);
    }

    @Test
    @DisplayName("When conference is 4 days away then increase quality with three")
    void when_ConferenceIsNineDaysAway_then_IncreaseQualityByThree() {
        final BackstagePass conference = createBackstagePass("DEVOXX ANTWERP", 9, 20);
        final BackstagePass conferenceExpected = createBackstagePass("DEVOXX ANTWERP", 8, 22);

        final Item actualConference = conferenceManager.manage(conference);

        Assertions.assertEquals(conferenceExpected, actualConference);
    }

    @Test
    @DisplayName("When conference is over then quality is 0")
    void when_ConferenceIsOver_then_QualityIsZero() {
        final BackstagePass conference = createBackstagePass("DEVOXX ANTWERP", 0, 20);
        final BackstagePass conferenceExpected = createBackstagePass("DEVOXX ANTWERP", -1, 0);

        final Item actualConference = conferenceManager.manage(conference);

        Assertions.assertEquals(conferenceExpected, actualConference);
    }
}
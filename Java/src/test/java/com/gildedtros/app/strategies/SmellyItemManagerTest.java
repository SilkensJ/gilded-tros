package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.GeneralItem;
import com.gildedtros.domain.model.SmellyItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.gildedtros.domain.factory.ItemFactory.createGeneralItem;
import static com.gildedtros.domain.factory.ItemFactory.createSmellyItem;

class SmellyItemManagerTest {

    public SmellyItemManager smellyItemManager = new SmellyItemManager();

    @Test
    @DisplayName("When a smelly item sell date is 20 days away then quality degrades always twice as fast when a day passes")
    void when_SmellyItemToSellIn20Days_then_decreaseQualityTwice() {
        final SmellyItem smellyItem = createSmellyItem("Spaghetti code", 20, 20);
        final SmellyItem smellyItemExpected = createSmellyItem("Spaghetti code", 19, 18);

        final Item actualSmellyItem = smellyItemManager.manage(smellyItem);

        Assertions.assertEquals(smellyItemExpected, actualSmellyItem);
    }

    @Test
    @DisplayName("When a smelly item has a quality of 1 then degrade quality with one")
    void when_SmellyItemHasQualityOfOne_then_decreaseQualityOnce() {
        final SmellyItem smellyItem = createSmellyItem("Spaghetti code", 20, 1);
        final SmellyItem smellyItemExpected = createSmellyItem("Spaghetti code", 19, 0);

        final Item actualSmellyItem = smellyItemManager.manage(smellyItem);

        Assertions.assertEquals(smellyItemExpected, actualSmellyItem);
    }

    @Test
    @DisplayName("When another item than a smelly item is passed then throw exception")
    void when_GeneralItemIsGiven_then_throwException() {
        final GeneralItem invalid = createGeneralItem("INVALID", 20, 20);

        Assertions.assertThrows(IllegalArgumentException.class, () -> smellyItemManager.manage(invalid));

    }
}
package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.GeneralItem;
import com.gildedtros.domain.model.Wine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.gildedtros.domain.factory.ItemFactory.createGeneralItem;
import static com.gildedtros.domain.factory.ItemFactory.createWine;

class WineManagerTest {

    public WineManager wineManager = new WineManager();

    @Test
    @DisplayName("When wine sell date is 20 days away then increase quality when a day passes")
    void when_WineToSellIn20Days_then_increaseQuality() {
        final Wine wine = createWine("MERLOT EXELLENT 2021 ", 20, 20);
        final Wine wineExpected = createWine("MERLOT EXELLENT 2021 ", 19, 21);

        final Item actualWine = wineManager.manage(wine);

        Assertions.assertEquals(wineExpected, actualWine);
    }

    @Test
    @DisplayName("When wine has a quality of 50 then it cannot increase anymore")
    void when_WineHasQualityOfFifty_then_dontIncrease() {
        final Wine wine = createWine("MERLOT EXELLENT 2021 ", 20, 50);
        final Wine wineExpected = createWine("MERLOT EXELLENT 2021 ", 19, 50);

        final Item actualWine = wineManager.manage(wine);

        Assertions.assertEquals(wineExpected, actualWine);
    }

    @Test
    @DisplayName("When another item than wine is passed then throw exception")
    void when_GeneralItemIsGiven_then_throwException() {
        final GeneralItem invalid = createGeneralItem("INVALID", 20, 20);

        Assertions.assertThrows(IllegalArgumentException.class, () -> wineManager.manage(invalid));

    }
}
package com.gildedtros;

import com.gildedtros.domain.factory.ItemFactory;
import com.gildedtros.domain.model.GeneralItem;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedTrosTest {

    @Test
    @DisplayName("when a generalItem has a quality of zero then don't degrade")
    void when_generalItemHasQualityZero_then_dontDegrade() {
        final GeneralItem generalItem = ItemFactory.createGeneralItem("foo", 0, 0);
        Item[] items = new Item[] {generalItem};
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals(generalItem, app.items[0]);
    }

    //TODO add more tests

}

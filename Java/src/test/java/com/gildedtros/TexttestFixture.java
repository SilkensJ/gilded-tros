package com.gildedtros;

import static com.gildedtros.domain.factory.ItemFactory.*;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("AXXES CODE KATA - GILDED TROS");

        Item[] items = new Item[]{
                createGeneralItem("Ring of Cleansening Code", 10, 20),
                createWine("Good Wine", 2, 0),
                createGeneralItem("Elixir of the SOLID", 5, 7),
                createLegendaryItem("B-DAWG Keychain"),
                createLegendaryItem("B-DAWG Keychain"),
                createBackstagePass("Backstage passes for Re:Factor", 15, 20),
                createBackstagePass("Backstage passes for Re:Factor", 10, 49),
                createBackstagePass("Backstage passes for HAXX", 5, 49),
                // these smelly items do not work properly yet
                createSmellyItem("Duplicate Code", 3, 6),
                createSmellyItem("Long Methods", 3, 6),
                createSmellyItem("Ugly Variable Names", 3, 6)
        };

        GildedTros app = new GildedTros(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }

}

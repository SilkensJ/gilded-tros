package com.gildedtros.domain.exception;

import com.gildedtros.domain.validation.ValidationError;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class ValidationException extends RuntimeException {
     private final List<ValidationError> validationErrors;

    public ValidationException(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public String getMessage() {
        return validationErrors.stream()
                .map(ValidationError::getErrorMessage)
                .collect(Collectors.joining(","));
    }
}

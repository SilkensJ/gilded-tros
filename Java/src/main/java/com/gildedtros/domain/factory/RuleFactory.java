package com.gildedtros.domain.factory;

import com.gildedtros.domain.validation.ItemValidator;
import com.gildedtros.domain.validation.rule.ValidateNameRule;
import com.gildedtros.domain.validation.rule.ValidateQualityRule;

public class RuleFactory {

    private RuleFactory() {
        throw new UnsupportedOperationException();
    }

    public static ValidateNameRule createValidateNameRule(){
        return new ValidateNameRule();
    }

    public static ValidateQualityRule createValidateQualityRule(){
        return new ValidateQualityRule();
    }

    public static ItemValidator createItemValidator(){
        return new ItemValidator();
    }
}

package com.gildedtros.domain.factory;

import com.gildedtros.Item;
import com.gildedtros.domain.exception.ValidationException;
import com.gildedtros.domain.model.BackstagePass;
import com.gildedtros.domain.model.GeneralItem;
import com.gildedtros.domain.model.LegendaryItem;
import com.gildedtros.domain.model.SmellyItem;
import com.gildedtros.domain.model.Wine;
import com.gildedtros.domain.validation.ValidationError;

import java.util.List;

import static com.gildedtros.domain.factory.RuleFactory.createItemValidator;

public class ItemFactory {


    private ItemFactory() {
        throw new UnsupportedOperationException(); // class should never be instantiated
    }

    public static Wine createWine(String name, Integer sellIn, Integer quality) {
        final Wine wine = Wine.builder().withName(name).withSellIn(sellIn).withQuality(quality).build();
        validateItem(wine);

        return wine;
    }

    public static LegendaryItem createLegendaryItem(String name) {
        final LegendaryItem legendaryItem = LegendaryItem.builder().withName(name).build();
        validateItem(legendaryItem);
        return legendaryItem;
    }

    public static SmellyItem createSmellyItem(String name, Integer sellIn, Integer quality) {
        final SmellyItem smellyItem = SmellyItem.builder().withName(name).withSellIn(sellIn).withQuality(quality).build();
        validateItem(smellyItem);
        return smellyItem;
    }

    public static BackstagePass createBackstagePass(String name, Integer sellIn, Integer quality) {
        final BackstagePass backstagePass = BackstagePass.builder().withName(name).withSellIn(sellIn).withQuality(quality).build();
        validateItem(backstagePass);
        return backstagePass;

    }

    public static GeneralItem createGeneralItem(String name, Integer sellIn, Integer quality) {
        final GeneralItem generalItem = GeneralItem.builder().withName(name).withSellIn(sellIn).withQuality(quality).build();
        validateItem(generalItem);
        return generalItem;
    }

    private static void validateItem(Item wine) {
        final List<ValidationError> validationErrors = createItemValidator().validate(wine);
        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors);
        }
    }

}

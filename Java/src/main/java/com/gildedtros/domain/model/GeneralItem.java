package com.gildedtros.domain.model;

import com.gildedtros.Item;

import java.util.Objects;

public class GeneralItem extends Item implements Degradable {

    private GeneralItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public static GeneralItemBuilder builder() {
        return new GeneralItemBuilder();
    }

    @Override
    public void degradeQuality() {
        if (quality != 0) {
            quality--;
        }
        if (sellIn < 0) {
            for (int i = 0; i < 2; i++) {
                if (i != 0) {
                    quality--;
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return sellIn == item.sellIn &&
                quality == item.quality &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sellIn, quality);
    }


    public static class GeneralItemBuilder {
        private String name;
        private Integer sellIn;
        private Integer quality;


        public GeneralItemBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public GeneralItemBuilder withSellIn(Integer sellIn) {
            this.sellIn = sellIn;
            return this;
        }

        public GeneralItemBuilder withQuality(Integer quality) {
            this.quality = quality;
            return this;
        }

        public GeneralItem build() {
            return new GeneralItem(name, sellIn, quality);
        }
    }
}

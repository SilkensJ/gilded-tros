package com.gildedtros.domain.model;

import com.gildedtros.Item;

import java.util.Objects;

public class SmellyItem extends Item implements Degradable {

    private SmellyItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public static SmellyItemBuilder builder() {
        return new SmellyItemBuilder();
    }

    @Override
    public void degradeQuality() {
        for (int i = 0; i < 2; i++) {
            if (quality != 0) {
                quality--;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return sellIn == item.sellIn &&
                quality == item.quality &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sellIn, quality);
    }

    public static class SmellyItemBuilder {
        private String name;
        private Integer sellIn;
        private Integer quality;


        public SmellyItemBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public SmellyItemBuilder withSellIn(Integer sellIn) {
            this.sellIn = sellIn;
            return this;
        }

        public SmellyItemBuilder withQuality(Integer quality) {
            this.quality = quality;
            return this;
        }

        public SmellyItem build() {
            return new SmellyItem(name, sellIn, quality);
        }
    }
}

package com.gildedtros.domain.model;

import com.gildedtros.Item;

import java.util.Objects;

public class Wine extends Item implements Increasable {
    private Wine(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public static WineBuilder builder(){
        return new WineBuilder();
    }

    @Override
    public void increaseQuality() {
        if (quality < 50) {
            quality++;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return sellIn == item.sellIn &&
                quality == item.quality &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sellIn, quality);
    }

    public static class WineBuilder {
        private String name;
        private Integer sellIn;
        private Integer quality;


        public WineBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public WineBuilder withSellIn(Integer sellIn) {
            this.sellIn = sellIn;
            return this;
        }

        public WineBuilder withQuality(Integer quality) {
            this.quality = quality;
            return this;
        }

        public Wine build() {
            return new Wine(name, sellIn, quality);
        }
    }
}

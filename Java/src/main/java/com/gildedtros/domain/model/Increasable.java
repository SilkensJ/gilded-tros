package com.gildedtros.domain.model;

public interface Increasable {
    void increaseQuality();
}

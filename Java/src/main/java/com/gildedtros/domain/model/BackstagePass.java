package com.gildedtros.domain.model;

import com.gildedtros.Item;

import java.util.Objects;

public class BackstagePass extends Item {

    private BackstagePass(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public static BackstagePassBuilder builder() {
        return new BackstagePassBuilder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return sellIn == item.sellIn &&
                quality == item.quality &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sellIn, quality);
    }


    public static class BackstagePassBuilder {
        private String name;
        private Integer sellIn;
        private Integer quality;


        public BackstagePassBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public BackstagePassBuilder withSellIn(Integer sellIn) {
            this.sellIn = sellIn;
            return this;
        }

        public BackstagePassBuilder withQuality(Integer quality) {
            this.quality = quality;
            return this;
        }

        public BackstagePass build() {
            return new BackstagePass(name, sellIn, quality);
        }
    }
}

package com.gildedtros.domain.model;

public interface Degradable {
    void degradeQuality();
}

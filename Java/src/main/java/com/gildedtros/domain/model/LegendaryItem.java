package com.gildedtros.domain.model;

import com.gildedtros.Item;

import java.util.Objects;

public class LegendaryItem extends Item {
    private LegendaryItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public static LegendaryBuilder builder(){
        return new LegendaryBuilder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return sellIn == item.sellIn &&
                quality == item.quality &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sellIn, quality);
    }

    public static class LegendaryBuilder {
        private String name;
        private Integer sellIn;
        private Integer quality;


        public LegendaryBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public LegendaryItem build() {
            return new LegendaryItem(name, 0, 80);
        }
    }
}

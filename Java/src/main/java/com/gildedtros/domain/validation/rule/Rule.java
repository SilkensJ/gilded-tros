package com.gildedtros.domain.validation.rule;

import com.gildedtros.Item;
import com.gildedtros.domain.validation.ValidationError;

import java.util.Optional;

public interface Rule {

    Optional<ValidationError> validate(Item item);
}

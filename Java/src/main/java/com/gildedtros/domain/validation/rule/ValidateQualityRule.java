package com.gildedtros.domain.validation.rule;

import com.gildedtros.Item;
import com.gildedtros.domain.model.LegendaryItem;
import com.gildedtros.domain.validation.ValidationError;

import java.util.Optional;

public class ValidateQualityRule implements Rule {

    @Override
    public Optional<ValidationError> validate(Item item) {

        if (item.quality < 0) {
            return Optional.of(new ValidationError("Quality cannot be negative!"));
        }
        if (!(item instanceof LegendaryItem) && item.quality > 50){
            return Optional.of(new ValidationError("Quality cannot be higher than 50!"));
        }
        return Optional.empty();
    }
}

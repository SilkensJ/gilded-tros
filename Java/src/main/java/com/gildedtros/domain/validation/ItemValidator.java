package com.gildedtros.domain.validation;

import com.gildedtros.Item;

import java.util.ArrayList;
import java.util.List;

import static com.gildedtros.domain.factory.RuleFactory.*;

public class ItemValidator {

    List<ValidationError> validationErrors = new ArrayList<>();

    public List<ValidationError> validate(Item item){
        createValidateNameRule()
                .validate(item)
                .map(validationError -> validationErrors.add(validationError));
        createValidateQualityRule()
                .validate(item)
                .map(validationError -> validationErrors.add(validationError));


       return validationErrors;
    }
}

package com.gildedtros.domain.validation.rule;

import com.gildedtros.Item;
import com.gildedtros.domain.validation.ValidationError;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public class ValidateNameRule implements Rule {

    @Override
    public Optional<ValidationError> validate(Item item) {

        if (StringUtils.isBlank(item.name)) {
            return Optional.of(new ValidationError("Name must be filled in!"));
        }
        return Optional.empty();
    }
}

package com.gildedtros.domain.validation;


import java.io.Serializable;

public class ValidationError implements Serializable {
    private static final long serialVersionUID = 2256477558314496007L;
    private final String errorMessage;


    public ValidationError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ValidationError{" +
                "errorMessage='" + errorMessage + '\'' +
                '}';
    }
}

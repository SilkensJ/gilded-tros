package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.BackstagePass;

public class ConferenceManager implements Manager {
    @Override
    public Item manage(Item item) {
        if (item instanceof BackstagePass) {
            item.sellIn--;
            if (item.sellIn < 0) {
                item.quality = 0;
            } else if (item.sellIn < 10 && item.sellIn > 5) {
                item.quality += 2;
            } else if (item.sellIn < 5) {
                item.quality += 3;
            } else {
                item.quality--;
            }
            return item;
        } else {
            throw new IllegalArgumentException("Sorry I cannot manage that!");
        }

    }
}

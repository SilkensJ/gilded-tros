package com.gildedtros.app.strategies;

import com.gildedtros.Item;

public interface Manager {
    Item manage(Item item);
}

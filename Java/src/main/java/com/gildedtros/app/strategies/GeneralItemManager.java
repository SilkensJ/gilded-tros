package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.GeneralItem;

public class GeneralItemManager implements Manager {
    @Override
    public Item manage(Item item) {
        if (item instanceof GeneralItem) {
            item.sellIn--;
            ((GeneralItem) item).degradeQuality();
            return item;
        } else {
            throw new IllegalArgumentException("Sorry I cannot manage that!");
        }
    }
}

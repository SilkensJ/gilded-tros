package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.SmellyItem;

public class SmellyItemManager implements Manager {
    @Override
    public Item manage(Item item) {
        if (item instanceof SmellyItem) {
            item.sellIn--;
            ((SmellyItem) item).degradeQuality();
            return item;
        } else {
            throw new IllegalArgumentException("Sorry I cannot manage that!");
        }
    }
}

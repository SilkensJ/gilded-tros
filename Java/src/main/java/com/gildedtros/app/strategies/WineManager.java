package com.gildedtros.app.strategies;

import com.gildedtros.Item;
import com.gildedtros.domain.model.Wine;

public class WineManager implements Manager {
    @Override
    public Item manage(Item item) {
        if (item instanceof Wine) {
            item.sellIn--;
            ((Wine) item).increaseQuality();
            return item;
        } else {
            throw new IllegalArgumentException("Sorry I cannot manage that!");
        }
    }
}

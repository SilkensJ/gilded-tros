package com.gildedtros.app.strategies;

import com.gildedtros.Item;

public class ItemManagers implements Manager {
    private final Manager manager;

    public ItemManagers(Manager manager) {
        this.manager = manager;
    }


    public Item manage(Item item) {
        return manager.manage(item);
    }
}

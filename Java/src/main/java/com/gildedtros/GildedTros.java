package com.gildedtros;

import com.gildedtros.app.strategies.*;
import com.gildedtros.domain.model.BackstagePass;
import com.gildedtros.domain.model.GeneralItem;
import com.gildedtros.domain.model.SmellyItem;
import com.gildedtros.domain.model.Wine;

import static java.util.Arrays.asList;

class GildedTros {
    Item[] items; // DO NOT EDIT

    private ItemManagers itemManagers;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        if (items != null) {
            asList(this.items).forEach(this::manage);
        }
    }

    private Item manage(Item item) {
        if (item instanceof GeneralItem) {
            this.itemManagers = new ItemManagers(new GeneralItemManager());
            return this.itemManagers.manage(item);
        }
        if (item instanceof SmellyItem) {
            this.itemManagers = new ItemManagers(new SmellyItemManager());
            return this.itemManagers.manage(item);
        }
        if (item instanceof Wine) {
            this.itemManagers = new ItemManagers(new WineManager());
            return this.itemManagers.manage(item);
        }
        if (item instanceof BackstagePass) {
            this.itemManagers = new ItemManagers(new ConferenceManager());
            return this.itemManagers.manage(item);
        }
        return item;
    }
}